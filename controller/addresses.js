var express = require('express');
var model = require('../model');
var router = express.Router();

/* Create Address */
router.post('/', (req,res)=>{
    var body=req.body;
    if(!body.hasOwnProperty("user_id")){
        res.json({
            stat:'failure',
            code:300,
            error:'userId cannot be null'
        })
    }
    body.userId=body.user_id;
    model.address.create(body).then(data=>{
        res.json({
            stat:'success',
            code:200,
            id: data.id
        })
    }, error=>{
        res.json({
            stat:'failure',
            code: 300,
            error:error
        })
    })
});
/*Update address*/
router.put('/:id', (req,res)=>{
   var body=req.body;
   delete body.id;
   delete body.userId;
   model.address.update(body,{
       where: {
           id: req.params.id
       }
   }).then(data=>{
       if(data>0){
            res.json({
                stat:'success',
                code:200
            })
       }else{
           res.json({
               stat:'nodata',
               code: 302
           })
       }
    }, error=>{
        res.json({
            stat:'failure',
            code: 300,
            error:error
        })
    })
});
/*Get address by id*/
router.get('/:id', (req,res)=>{
    model.address.findOne({
        where:{
            id: req.params.id
        }
    }).then(data=>{
        res.json({
            stat:'success',
            code:200,
            address:data
        })
    }, error=>{
        res.json({
            stat:'failure',
            code: 300,
            error:error
        })
    })
});
/*Get all address for user*/
router.get('/all/:id', (req,res)=>{
    model.address.findAll({
        where:{
            userId: req.params.id
        }
    }).then(data=>{
        res.json({
            stat:'success',
            code:200,
            address_count: data.length,
            addresses:data
        })
    }, error=>{
        res.json({
            stat:'failure',
            code: 300,
            error:error
        })
    })
});
module.exports=router;
