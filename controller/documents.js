var express = require('express');
var model = require('../model');
var multer  = require('multer');
var crypto=require('crypto');
var mime = require('mime');
var cors = require('cors');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './documents/')
  },
  filename: function (req, file, cb) {
    crypto.pseudoRandomBytes(16, function (err, raw) {
      cb(null, raw.toString('hex') + '.' + mime.extension(file.mimetype));
    });
  }
});
var upload = multer({ storage: storage });
var router = express.Router();
router.use(cors());
/* User docs */
router.post('/upload/user/:id', upload.single('file'), (req,res)=>{
  model.document.create({
    file_path:req.file.path,
    file_name: req.file.originalname,
    userId:req.params.id,
    expiry_date: req.body.expiry_date
  }).then(data=>{
    res.json({
      path: req.file.path,
      file_name: req.file.filename,
      size: req.file.size,
      stat: 'success',
      code: 200
    })
    console.log(req.file);
  }, error=>{
    res.json({
      stat: 'error',
      code: 300
    });
    console.log(error);
  })
});
/* Get user docs */
router.get('/user/:id', (req,res)=>{
    model.document.findAll({
        where:{
            userId: req.params.id
        }
    }).then(data=>{
        res.json({
            stat:'success',
            code:200,
            docs_count:data.length,
            docs:data
        })
    }, error=>{
        res.json({
            stat:'failure',
            code:300,
            error:error
        })
    })
});
/* Driver docs */
router.post('/upload/driver/:id', upload.single('file'), (req,res)=>{
  model.document.create({
    file_path:req.file.path,
    file_name: req.file.originalname,
    driverId:req.params.id,
    expiry_date: req.body.expiry_date
  }).then(data=>{
    res.json({
      path: req.file.path,
      file_name: req.file.filename,
      size: req.file.size,
      stat: 'success',
      code: 200
    })
    console.log(req.file);
  }, error=>{
    res.json({
      stat: 'error',
      code: 300,
      error: error
    });
    console.log(error);
  })
});
/* Get Driver docs */
router.get('/driver/:id', (req,res)=>{
    model.document.findAll({
        where:{
            userId: req.params.id
        }
    }).then(data=>{
        res.json({
            stat:'success',
            code:200,
            docs_count:data.length,
            docs:data
        })
    }, error=>{
        res.json({
            stat:'failure',
            code:300,
            error:error
        })
    })
});
/* Vehicle docs */
router.post('/upload/vehicle/:id', upload.single('file'), (req,res)=>{
  model.document.create({
    file_path:req.file.path,
    file_name: req.file.originalname,
    vehicleId:req.params.id,
    expiry_date: req.body.expiry_date
  }).then(data=>{
    res.json({
      path: req.file.path,
      file_name: req.file.filename,
      size: req.file.size,
      stat: 'success',
      code: 200
    })
    console.log(req.file);
  }, error=>{
    res.json({
      stat: 'error',
      code: 300
    });
    console.log(error);
  })
});
/* Get Driver docs */
router.get('/vehicle/:id', (req,res)=>{
    model.document.findAll({
        where:{
            vehicleId: req.params.id
        }
    }).then(data=>{
        res.json({
            stat:'success',
            code:200,
            docs_count:data.length,
            docs:data
        })
    }, error=>{
        res.json({
            stat:'failure',
            code:300,
            error:error
        })
    })
});
module.exports=router;
