var express = require('express');
var model = require('../model');
var router = express.Router();
/* Create a new role */
router.post('/', (req, res) => {
    var body = req.body;
    model.role.create({
        role_name: body.role_name,
        module_ids: body.module_ids,
        controller_name: body.controller_name
    }).then(data => {
        res.json({
            id: data.id,
            stat: 'success',
            code: 201
        });
    }, error => {
        res.json({
            stat: 'failure',
            code: 300
        });
        console.log(error);
    })
});
/* Get all roles */
router.get('/', (req, res) => {
        model.role.findAll().then(data => {
            res.json({
                role_count: data.length,
                roles: data
            })
        }, error => {
            res.json({
                stat: 'failure',
                code: 301
            })
        })
    })
    /* Get Role by id */
router.get('/:id', (req, res) => {
        console.log(req.params.id);
        model.role.findOne({
          where:{
            id: req.params.id
          }
        }).then(data => {
            (data == null) ? res.json({
                stat: 'nodata',
                code: 400
            }): res.json({
                role: data,
                code: 200
            })
        }, error => {
            res.json({
                stat: 'failure',
                code: 301
            })
        })
    })
    /* Delete a Role */
router.delete('/:id', (req, res) => {
        /*Check if role exists in admini users table*/

        //If role is unused the delete
        model.role.destroy({
            where: {
                id: req.params.id
            }
        }).then(data => {
            (data == 0) ? res.json({
                stat: 'nodata',
                code: 400
            }): res.json({
                stat: 'success',
                code: 300
            })
        }, error => {
            res.json({
                stat: 'failure',
                code: 300
            })
        })
    })
    /*Update a role*/
router.put('/:id', (req, res) => {
    var body = req.body;
    var role_name = body.role_name;
    var module_ids = body.module_ids;
    var controller_name=body.controller_name;
    model.role.update({
        role_name: role_name,
        module_ids: module_ids,
        controller_name: controller_name
    }, {
        where: {
            id: req.params.id
        }
    }).then(data => {
        (data == 0) ? res.json({
            stat: 'nodata',
            code: 400
        }): res.json({
            stat: 'success',
            code: 300
        })
    }, error => {
        res.json({
            stat: 'failure',
            code: 300
        })
    })
});
/* Get modules */
router.get('/modules/:id', (req,res)=>{
    model.role.findOne({
      where:{
        id: req.params.id
      },
      attributes:['id','module_ids']
    }).then(data => {
        (data == null) ? res.json({
            stat: 'nodata',
            code: 400
        }): res.json({
            stat:'success',
            id: data.id,
            module_ids: data.module_ids.split(','),
            code: 200
        })
    }, error => {
        res.json({
            stat: 'failure',
            code: 301
        })
    })
});
module.exports = router;
