var express = require('express');
var model = require('../model');
var router = express.Router();
var multer  = require('multer');
var crypto=require('crypto');
var cors = require('cors');
var mime = require('mime');
/********************************************************/
/***************FILE UPLOAD CONFIG***********************/
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads/')
  },
  filename: function (req, file, cb) {
    crypto.pseudoRandomBytes(16, function (err, raw) {
      cb(null, raw.toString('hex') + '.' + mime.extension(file.mimetype));
    });
  }
});
var upload = multer({ storage: storage });
/**************CONFIG ENDS*******************************/
/********************************************************/
router.use(cors());
router.use(cors());
/* Register a new driver */

router.post('/',upload.single('driver_photo'), (req,res)=>{
    var body=req.body;
    model.driver.create({
      fleetOwnerId: body.fleet_owner_id,
      driver_name:body.driver_name,
      driver_address:body.driver_address,
      driver_mobile:body.driver_phone,
      driver_photo_path:req.file.filename
    }).then(data => {
        res.json({
            id: data.id,
            stat: 'success',
            code: 200
        })
    }, error => {
        res.json({
            stat: 'failure',
            code: 300
        })
        console.log(error);
    })
});

router.post('/docs',(req,res)=>{
  var body=req.body;
  var driver_docs=body.driver_docs;
  driver_docs.forEach(function(item){
    item['driverId']=body.driver_id;
  })
  model.driver.findOne({
    where:{
      id: body.driver_id
    }
  }).then(data1=>{
    if(data1!=null){
      model.driver_doc.bulkCreate(driver_docs).then(data=>{
        res.json({
          stat:'success',
          code: 200,
          count: driver_docs.length
        })
      }, error=>{
        res.json({
            stat: 'failure',
            message:'could not upload document data',
            code: 300
        })
      });
    }else{
      res.json({
        stat: 'nodata',
        code: 301
      })
    }
  }, error1=>{
    res.json({
        stat: 'failure',
        code: 300
    })
    console.log(error1);
  })
})

router.post('/pair', (req,res)=>{
  var body=req.body;
  model.driver_vehicle_pair.create({
    vehicleId:body.vehicle_id,
    driverId:body.driver_id,
    fleetOwnerId:body.fleet_owner_id
  }).then(data => {
      res.json({
          id: data.id,
          stat: 'success',
          code: 201
      });
  }, error => {
      res.json({
          stat: 'failure',
          code: 300
      });
      console.log(error);
  });
});

/*Get a driver by id*/
router.get('/:id',(req,res)=>{
  var id=req.params.id;
  model.driver.findOne({
    where: {
      id: id
    },
    include:[model.driver_doc,{
      model: model.driver_vehicle_pair,
      include:[model.vehicle]
    }]
  }).then(data=>{
    data?res.json({
      stat: 'success',
      code: 200,
      driver: data
    }):res.json({
      stat:'nodata',
      code: 301
    })
  }, error=>{
    res.json({
      stat:'failure',
      code: 300,
      error: error
    })
    console.log(error);
  })
});
/*Get all drivers*/
router.get('/',(req,res)=>{
  var id=req.params.id;
  model.driver.findAll({
    include:[model.driver_doc,{
      model: model.driver_vehicle_pair,
      include:[model.vehicle]
    }]
  }).then(data=>{
    res.json({
      driver_count: data.length,
      drivers: data
    })
  }, error=>{
    res.json({
      stat:'failure',
      code: 300,
      error: error
    })
    console.log(error);
  })
});

/*Update Rating*/
router.post('/rating', (req,res)=>{
  var body=req.body;
  model.driver.findOne({
    where: {
      id: body.driver_id
    },
    attributes:['rating','rating_count']
  }).then(data=>{
    if(data==null){
      res.json({
        stat: 'nodata',
        code: 302
      });
    }else{
      var rating=data.rating;
      var rating_count=data.rating_count;
      var cur_tot=(rating*rating_count)+body.rating;
      var avg=cur_tot/(rating_count+1);
      model.driver.update({
        rating: avg.toFixed(1),
        rating_count: rating_count+1
      },{
        where: {
          id: body.driver_id
        }
      }).then(data1=>{
        res.json({
          stat:'success',
          code: 200,
          updated_rating: data1.rating,
          id: data1.id
        })
      }, error1=>{
        res.json({
          stat:'failure',
          code: 300,
          error: error1
        })
      })
    }
  }, error=>{
    res.json({
      stat: 'failure',
      code: 300,
      error: error1
    })
  })
})

module.exports = router;
