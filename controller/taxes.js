var express = require('express');
var model = require('../model');
var router = express.Router();

router.post('/', (req,res)=>{
  var body=req.body;
  if(body.hasOwnProperty('id')){
    delete body.id;
  }
  model.tax.create(body).then(data=>{
    res.json({
      stat:'success',
      code:200,
      id:data.id
    });
  },error=>{
    res.json({
      stat:'failure',
      code:300,
      error:error
    })
  })
});
/* Latest tax for today */
router.get('/latest', (req,res)=>{
  model.tax.findOne({
    where:{
      valid_from:{
        $or:{
          $lt: new Date(),
          $eq: new Date()
        }
      }
    },
    order:[
      ['valid_from','DESC']
    ]
  }).then(data=>{
    if(data!=null){
      res.json({
        stat:'success',
        code:200,
        data:data
      });
    }else{
      res.json({
        stat:'nodata',
        code:302
      });
    }
  }, error=>{
    console.log(error);
    res.json({
      stat:'failure',
      code:300,
      error:error
    })
  });
})
/* Get by id */
router.get('/:id', (req,res)=>{
  model.tax.findOne({
    where: {
      id:req.params.id
    }
  }).then(data=>{
    if(data!=null){
      res.json({
        stat:'success',
        code:200,
        data:data
      });
    }else{
      res.json({
        stat:'nodata',
        code:302
      });
    }
  }, error=>{
    console.log(error);
    res.json({
      stat:'failure',
      code:300,
      error:error
    })
  });
})
/*Get all taxes*/
router.get('/',(req,res)=>{
  model.tax.findAll().then(data=>{
    res.json({
      stat:'success',
      code:200,
      tax_count:data.length,
      taxes:data
    });
  }, error=>{
    console.log(error);
    res.json({
      stat:'failure',
      code:300,
      error:error
    })
  });
})
/*Update tax data*/
router.put('/:id', (req,res)=>{
  var body=req.body;
  if(body.hasOwnProperty('id')){
    delete body.id;
  }
  model.tax.update(body,{
    where:{
      id:req.params.id
    }
  }).then(data=>{
    if(data>0){
      res.json({
        stat:'success',
        code:200
      });
    }else{
      res.json({
        stat:'nodata',
        code:302
      });
    }
  }, error=>{
    console.log(error);
    res.json({
      stat:'failure',
      code:300,
      error:error
    })
  });
})
module.exports = router;
