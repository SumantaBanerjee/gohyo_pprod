var express = require('express');
var model = require('../model');
var router = express.Router();

/* new concern : user */
router.post('/user', (req,res)=>{
    model.concern.create({
        concern: req.body.concern,
        concern_type: req.body.concern_type,
        orderId: req.body.order_id,
        userId: req.body.user_id
    }).then(data=>{
        res.json({
            stat:'success',
            code:200,
            concern_id: data.id
        })
    }, error=>{
        res.json({
            stat:'nodata',
            code:300,
            error:error
        })
    })
});
/* new concern : driver */
router.post('/driver', (req,res)=>{
    model.concern.create({
        concern: req.body.concern,
        concern_type: req.body.concern_type,
        orderId: req.body.order_id,
        driverId: req.body.driver_id
    }).then(data=>{
        res.json({
            stat:'success',
            code:200,
            concern_id: data.id
        })
    }, error=>{
        res.json({
            stat:'nodata',
            code:300,
            error:error
        })
    })
});
/* get concerns for order id */
router.get('/get/:id', (req,res)=>{
   var id=req.params.id;
   model.concern.findAll({
       where: {
           orderId:id
       },
       include:[model.order]
   }).then(data=>{
       res.json({
           stat:'success',
           code:200,
           concern_count:data.length,
           concerns: data
       })
   }, error=>{
       res.json({
           stat:'failure',
           code:300,
           error:error
       })
   })
});
/* get concerns for order id and userid */
router.get('/get/user/:order/:user', (req,res)=>{
   model.concern.findAll({
       where: {
           orderId:req.params.order,
           userId:req.params.user
       }
   }).then(data=>{
       res.json({
           stat:'success',
           code:200,
           concern_count:data.length,
           concerns: data
       })
   }, error=>{
       res.json({
           stat:'failure',
           code:300,
           error:error
       })
   })
});
/* get concerns for order id and driverid */
router.get('/get/driver/:order/:driver', (req,res)=>{
   model.concern.findAll({
       where: {
           orderId:req.params.order,
           driverId:req.params.driver
       }
   }).then(data=>{
       res.json({
           stat:'success',
           code:200,
           concern_count:data.length,
           concerns: data
       })
   }, error=>{
       res.json({
           stat:'failure',
           code:300,
           error:error
       })
   })
});
/*Get concerned orders*/
router.get('/all', (req,res)=>{
   model.concern.findAll({
       attributes:[],
       include:[model.order]
   }).then(data=>{
       res.json({
           stat:'success',
           code:200,
           concern_count:data.length,
           concerns: data
       })
   }, error=>{
       res.json({
           stat:'failure',
           code:300,
           error:error
       })
   })
});
module.exports=router;
