var express = require('express');
var model = require('../model');
var router = express.Router();

/* Create Zones */
router.post('/', (req, res) => {
    var body = req.body;
    var literal = "ST_GeographyFromText('point(" + body.lat + " " + body.lng + ")')"
    var point = {
        type: 'Geography',
        coordinates: [body.lat, body.lng],
        crs: {
            type: 'name',
            properties: {
                name: 'EPSG:4326'
            }
        }
    };
    model.zone.create({
        zone_name: body.zone_name,
        zone_location: model.sequelize.literal(literal),
        loading_time: body.loading_time,
        pincode: body.pincode,
        district:body.district,
        city:body.city,
        unloading_time: body.unloading_time,
        rate_per_minute: body.rate_per_minute
    }).then(data => {
        res.json({
            stat: 'success',
            zone_id: data.id,
            code: 200
        })
    }, error => {
        console.log(error);
        res.json({
            stat: 'failure',
            code: 300
        })
    })
});
/*Get all zones*/
router.get('/all', (req, res) => {
    model.zone.findAll().then(data => {
        res.json({
            zone_count: data.length,
            zones: data,
            stat: 'success',
            code: 200
        })
    }, error => {
        res.json({
            stat: 'failure',
            code: 300
        })
    });
});
/*Get specific zone*/
router.get('/:id', (req, res) => {
    model.zone.findOne({
        where: {
            id: req.params.id
        }
    }).then(data => {
        if (data == null) {
            res.json({
                stat: 'nodata',
                code: 302
            })
        } else {
            res.json({
                zones: data,
                stat: 'success',
                code: 200
            });
        }
    }, error => {
        res.json({
            stat: 'failure',
            code: 300,
            error: error
        });
    });
});
/*Get zones by name*/
router.get('/byname/:name', (req,res)=>{
  var name=req.params.name;
  model.sequelize.query('select * from "zones" where soundex("zone_name")=soundex(\''+name+'\')',{
    type: model.sequelize.QueryTypes.SELECT
  }).then(zones=>{
    res.json({
      stat:'success',
      code:200,
      zones_count:zones.length,
      zones:zones
    })
  }, error=>{
    res.json({
      stat:'failure',
      code: 300,
      error:error
    })
  })
});
/* Get zone by pincode and name */
router.get('/bypin/:name/:pin', (req,res)=>{
  var name=req.params.name;
  model.zone.findOne({
      where:{
          zone_name: req.params.name,
          pincode: req.params.pin
      }
  }).then(zone=>{
    if(zone){
        res.json({
          stat:'success',
          code:200,
          zone:zone
        })
    }else{
        res.json({
          stat:'nodata',
          code:302
        }) 
    }
  }, error=>{
    res.json({
      stat:'failure',
      code: 300,
      error:error
    })
  })
});
/* Update Zone Data */
router.put('/:id', (req,res)=>{
  var body=req.body;
  if(body.hasOwnProperty('lat') && body.hasOwnProperty('lng')){
    var literal = "ST_GeographyFromText('point(" + body.lat + " " + body.lng + ")')";

    var point = {
        type: 'Geography',
        coordinates: [body.lat, body.lng],
        crs: {
            type: 'name',
            properties: {
                name: 'EPSG:4326'
            }
        }
    };
    body.zone_location = model.sequelize.literal(literal);
  }

  model.zone.update(body,{
    where:{
      id:req.params.id
    }
  }).then(data => {
    if(data!=null){
      res.json({
          stat: 'success',
          code: 200
      })
    }else{
      res.json({
          stat: 'nodata',
          code: 302
      })
    }
  }, error => {
      console.log(error);
      res.json({
          stat: 'failure',
          code: 300
      })
  })
});
/* Zones within */
router.post('/within/:radius', (req,res)=>{
  var body=req.body;
  var lat=body.lat;
  var lng=body.lng;
  var literal='point('+lat+' '+lng+')';
  model.zone.findAll({
    where:model.sequelize.where(model.sequelize.fn('ST_DWithin',model.sequelize.col('zone_location'),model.sequelize.fn('ST_GeogFromText',literal),req.params.radius),'t'),
    attributes:['id', 'zone_name'],
    include:[model.vehicle]
  }).then(data=>{
    res.json({
        stat:'success',
        code:200,
        zone_count:data.length,
        zones:data
    });
  },error=>{
      res.json({
          stat:'failure',
          code:300,
          error:error
      })
  })
  /*model.vehicle.findAll({
    where:{
      availability:'A'
    },
    include:[{
      model:model.zone,
      where:model.sequelize.where(model.sequelize.fn('ST_DWithin',model.sequelize.col('zone_location'),model.sequelize.fn('ST_GeogFromText','point(22.635950 88.437648)'),req.params.radius),'t'),
      attributes:['id', 'zone_name']
    }]
  }).then(data=>{
    res.json(data);
  })*/
});
/* Predict Zone by lat lng */
router.post('/demo/:radius', (req,res)=>{
    var lat=req.body.lat;
    var lng=req.body.lng;
    var id=req.params.id;
    var literal='point('+lat+' '+lng+')';
    model.zone.findOne({
        attributes:['id','zone_name','zone_location',model.sequelize.fn('ST_Distance',model.sequelize.col('zone_location'),model.sequelize.fn('ST_GeogFromText',literal))],
        order:[
            [model.sequelize.fn('ST_Distance',model.sequelize.col('zone_location'),model.sequelize.fn('ST_GeogFromText',literal))]
        ]
    }).then(data=>{
        res.json(data.id);
    },error=>{
        res.json(error);
    })
});
module.exports = router;
