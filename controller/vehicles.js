var express = require('express');
var model = require('../model');
var router = express.Router();
/* Create new vehicle */
router.post('/', (req, res) => {
    var body = req.body;
    model.vehicle.create({
        vehicleCategoryId: body.category_id,
        vehicle_make_name: body.vehicle_make_name,
        vehicle_number: body.vehicle_number,
        location: model.Sequelize.literal("point(0,0)"),
        schemeId: body.scheme_id,
        availability: 'V',
        fleetOwnerId: body.fleet_owner_id,
        vehicle_info: body.vehicle_info
    }).then(data => {
        res.json({
            stat: 'success',
            code: 200,
            id: data.id
        })
    }, error => {
        res.json({
            stat: 'failure',
            code: 300
        })
        console.log(error);
    })
});
/*Get all*/
router.get('/', (req, res) => {
    model.vehicle.findAll().then(data => {
        res.json({
            vehicles_count: data.length,
            vehicles: data
        })
    }, error => {
        res.json({
            stat: 'failure',
            code: 300
        })
    })
});
/*Get one*/
router.get('/:id', (req, res) => {
    model.vehicle.findOne({
        where: {
            id: req.params.id
        },
        include: [model.driver, {
            model:model.user,
            attributes: {
                exclude: ['password']
            }
        }]
    }).then(data => {
        data ? res.json({
            stat: 'success',
            vehicle: data
        }) : res.json({
            stat: 'nodata',
            code: 302
        })
    }, error => {
        res.json({
            stat: 'failure',
            code: 300
        })
        console.log(error);
    });
});
/*Update location*/
router.post('/locate', (req, res) => {
    var vehicle_id = req.body.vehicle_id;
    var lat = req.body.lat;
    var lng = req.body.lng;
    //var zoneId=req.body.zone_id;
    var literal = "point(" + lat + "," + lng + ")";
    console.log(literal)
    var literalx='point('+lat+' '+lng+')';
    model.zone.findOne({
        attributes:['id','zone_name','zone_location',model.sequelize.fn('ST_Distance',model.sequelize.col('zone_location'),model.sequelize.fn('ST_GeogFromText',literalx))],
        order:[
            [model.sequelize.fn('ST_Distance',model.sequelize.col('zone_location'),model.sequelize.fn('ST_GeogFromText',literalx))]
        ]
    }).then(zone=>{
        console.log("Zone Found: "+zone.id);
        model.vehicle.update({
            location: model.Sequelize.literal(literal),
            zoneId:zone.id
        }, {
            where: {
                id: vehicle_id
            }
        }).then(data => {
            data ? res.json({
                stat: 'success',
                code: 400
            }) : res.json({
                stat: 'nodata',
                code: 301
            })
        }, error => {
            res.json({
                stat: 'failure',
                code: 300,
                error:error
            })
        })
    },error2=>{
        res.json({
            stat:'failure',
            code:300,
            error:error
        });
    })
        
});
/*Get location*/
router.get('/locate/:id', (req, res) => {
    model.vehicle.findOne({
        where: {
            id: req.params.id
        },
        attributes: ['location', 'availability', 'zoneId']
    }).then(data => {
        data ? res.json({
            stat: 'success',
            location: data.location,
            availability: data.availability,
            code: 200
        }) : res.json({
            stat: 'nodata',
            code: 302
        })
    }, error => {
        res.json({
            stat: 'failure',
            code: 300
        })
    })
});
/* Demo zone Update */
router.post('/demo_zone/:vid/:zid', (req,res)=>{
    model.vehicle.update({
        zoneId:req.params.zid
    },{
        where:{
            id:req.params.vid
        }
    }).then(data=>{
        if(data==null){
            res.json({
                stat:'nodata',
                code:302
            })
        }else{
            res.json({
                stat:'success',
                code:200
            })
        }
        
    }, error=>{
        res.json({
            stat:'failure',
            code:300
        })
    })
})
/* Demo Location Update via pincode */
router.get('/demo_locate', (req, res) => {
    model.zone.findOne({
        where:{
            zone_name:req.body.pincode
        }
    }).then(zone=>{
        var vehicle_id = req.body.vehicle_id;
        var lat = req.body.lat;
        var lng = req.body.lng;
        var literal = "point(" + lat + "," + lng + ")";
        console.log(literal)
        model.vehicle.update({
            location: model.Sequelize.literal(literal),
            zoneId:zone.id
        }, {
            fields: ['location', 'id'],
            where: {
                id: vehicle_id
            }
        }).then(data => {
            data ? res.json({
                stat: 'success',
                code: 400
            }) : res.json({
                stat: 'success',
                code: 301
            })
        }, error => {
            res.json({
                stat: 'failure',
                code: 300
            })
        })
    }, error=>{
        res.json({
            stat: 'failure',
            code: 300
        })
    });
});
/*Update availability*/
router.post('/status', (req, res) => {
    var vehicle_id = req.body.vehicle_id;
    var code = req.body.status; //A or V
    model.vehicle.update({
        availability: code.toUpperCase()
    }, {
        fields: ['availability', 'id'],
        where: {
            id: vehicle_id
        }
    }).then(data => {
        data ? res.json({
            stat: 'success',
            code: 400
        }) : res.json({
            stat: 'success',
            code: 301
        })
    }, error => {
        res.json({
            stat: 'failure',
            code: 300
        })
    })
})

/*Get pair information*/ // --REMOVED
router.get('/pair/:id', (req, res) => {
    model.driver_vehicle_pair.findOne({
        where: {
            id: req.params.id
        },
        include: [model.driver, model.vehicle, {
            model: model.user,
            attributes: {
                exclude: ['password']
            },
            include: [model.user_category]
        }]
    }).then(data => {
        data ? res.json({
            stat: 'success',
            pair: data
        }) : res.json({
            stat: 'nodata',
            code: 302
        })
    }, error => {
        res.json({
            stat: 'failure',
            code: 300
        })
        console.log(error);
    })
});

/*Get all vehicle and driver pairs by owner id*/  // --REMOVED
router.get('/pair/owner/:id', (req, res) => {
    model.driver_vehicle_pair.findAll({
        where: {
            fleetOwnerId: req.params.id
        },
        include: [model.vehicle, model.driver]
    }).then(data => {
        res.json({
            stat: data.length > 0 ? 'success' : 'nodata',
            code: 200,
            pairs: data.length > 0 ? data : undefined
        });
    }, error => {
        res.json({
            stat: 'failure',
            code: 300,
            error: error
        })
    })
});

/*Get Zones*/
/*router.get('/zones', (req, res) => {
    model.zone.findAll().then(data => {
        res.json({
            stat: 'success',
            code: 200,
            zones: data
        })
    }, error => {
        res.json({
            stat: 'failure',
            code: 300
        })
    })
});*/
/*Get One Zone*/
/*router.get('/zones/:id', (req, res) => {
    model.zone.findOne({
        where: {
            id: req.params.id
        }
    }).then(data => {
        if (data) {
            res.json({
                stat: 'success',
                code: 200,
                zone: data
            })
        } else {
            res.json({
                stat: 'nodata',
                code: 302
            })
        }
    }, error => {
        res.json({
            stat: 'failure',
            code: 300
        })
    })
});*/
/*Create Zone*/
/*router.post('/zones', (req, res) => {
    var body = req.body;
    var lat = body.lat;
    var lng = body.lng;
    var literal = "point(" + lat + "," + lng + ")";
    model.zone.create({
        zone_name: body.zone_name,
        zone_location: model.Sequelize.literal(literal)
    }).then(data => {
        res.json({
            stat: 'success',
            code: 200,
            id: data.id
        })
    }, error => {
        res.json({
            stat: 'failure',
            code: 300
        })
    })
});*/
/*Find zones within radius*/
/*router.get('/zones/:radius', (req, res) => {

});*/

/*Get vehicles within radius*/
router.post('/within/:radius', (req,res)=>{
  var body=req.body;
  var lat=body.lat;
  var lng=body.lng;
  var literal='point('+lat+' '+lng+')';
  model.vehicle.findAll({
    where:{
      availability:'A'
    },
    include:[{
      model:model.zone,
      where:model.sequelize.where(model.sequelize.fn('ST_DWithin',model.sequelize.col('zone_location'),model.sequelize.fn('ST_GeogFromText',literal),req.params.radius),'t'),
      attributes:['id', 'zone_name']
    }]
  }).then(data=>{
    res.json({
      stat:'success',
      code:200,
      vehicle_count:data.length,
      vehicles:data
    });
  }, error=>{
    res.json({
      stat:'failure',
      code:300,
      error:error
    })
  })
});

/* Demo API for range query */
router.post('/within/byCategory/:radius', (req,res)=>{
  var body=req.body;
  var lat=body.lat;
  var lng=body.lng;
  var literal='point('+lat+' '+lng+')';
  model.vehicle_category.findAll({
      include:[{
        model:model.vehicle,
        where:{
        availability:'A'
        },
        include:[{
          model:model.zone,
          where:model.sequelize.where(model.sequelize.fn('ST_DWithin',model.sequelize.col('zone_location'),model.sequelize.fn('ST_GeogFromText',literal),req.params.radius),'t'),
          attributes:['id', 'zone_name']
        }]
      }]
  }).then(data=>{
      model.vehicle_category.findAll().then(categories=>{
          for(var i=0;i<categories.length;i++){
              for(var j=0;j<data.length;j++){
                  if(data[j].id==categories[i].id){
                      categories[i]=data[j];;
                  }
              }
          }
          for(var i=0;i<categories.length;i++){
              if(typeof categories[i].vehicles=="undefined"){
                  console.log("Vehicles undefined: " + categories[i].id);
                  var v=JSON.stringify(categories[i]);
                  v=JSON.parse(v);
                  v.vehicles=[];
                  categories[i]=v;
              }
          }
          res.json({
            stat:'success',
            code:200,
            category_count:categories.length,
            data:categories
          });
      }, error=>{
          res.json({
            stat:'nodata',
            code:302,
            error:error
          })
      });
  }, error=>{
    res.json({
      stat:'failure',
      code:300,
      error:error
    })
  })
});

/* Category APIs */
router.post('/category', (req, res) => {
    var body = req.body;
    if (body.hasOwnProperty('id')) {
        delete body.id;
    }
    model.vehicle_category.create(req.body).then(data => {
        res.json({
            stat: 'success',
            code: 200,
            id: data.id
        }), error => {
            res.json({
                stat: 'failure',
                code: 300,
                error: error
            })
        }
    })
})
/*Get One Category*/
router.get('/category/:id', (req,res, next)=>{
  var id=req.params.id;
  if(isNaN(id)){
    return next();
  }
  model.vehicle_category.findOne({
    where:{
      id: id
    }
  }).then(data=>{
    if(data==null){
      res.json({
        stat:'nodata',
        code: 302
      })
    }else{
      res.json({
        stat:'success',
        code:200,
        data:data
      })
    }
  }, error=>{
    res.json({
        stat: 'failure',
        code: 300,
        error: error
    })
  })
})
/* Get All Categories*/
router.get('/category/all', (req,res)=>{
  model.vehicle_category.findAll().then(data=>{
    res.json({
      stat:'success',
      code:200,
      category_count: data.length,
      categories:data
    })
  }, error=>{
    res.json({
      stat:'failure',
      code: 300,
      error:error
    })
  })
});
/*Get active order*/
router.get('/active_orders/:id', (req,res)=>{
    model.order.findAll({
        where:{
            vehicleId:req.params.id
        },
        include:[{
            model: model.timestamp,
            attributes:['currentStatus']
        }]
    }).then(data=>{
        res.json({
            stat:'success',
            code:200,
            order_count: data.length,
            orders: data
        })
    }, error=>{
        res.json({
            stat:'failure',
            code:300,
            error:error
        })
    })
});

module.exports = router;
