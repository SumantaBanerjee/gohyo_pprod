var express = require('express');
var model = require('../model');
var passwordHash = require('password-hash');
var multer  = require('multer');
var crypto=require('crypto');
var cors = require('cors');
var router = express.Router();
var mime = require('mime');
/********************************************************/
/***************FILE UPLOAD CONFIG***********************/
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads/')
  },
  filename: function (req, file, cb) {
    crypto.pseudoRandomBytes(16, function (err, raw) {
      cb(null, raw.toString('hex') + '.' + mime.extension(file.mimetype));
    });
  }
});
var upload = multer({ storage: storage });
/**************CONFIG ENDS*******************************/
/********************************************************/
router.use(cors());
/* Create a new user category */
router.post('/categories', (req, res) => {
    var body = req.body;
    model.user_category.create({
        user_category_name: body.category_name,
        module_ids: body.module_ids
    }).then(data => {
        res.json({
            stat: 'success',
            code: 201,
            id: data.id
        });
    }, error => {
        res.json({
            stat: 'failure',
            code: 300,
            message: error
        })
    })
})

/* Get all categories */
router.get('/categories', (req, res) => {
    model.user_category.findAll().then(data => {
        res.json({
            category_count: data.length,
            categories: data
        })
    }, error => {
        res.json({
            stat: 'failure',
            code: 301,
            message: error
        })
    })
})

/* Get category by id */
router.get('/categories/:id', (req, res) => {
    model.user_category.findOne({
        where: {
            id: req.params.id
        }
    }).then(data => {
        res.json({
            category: data,
            stat: data ? 200 : 302
        })
    }, error => {
        res.json({
            code: 301,
            stat: 'error',
            message: error
        })
    })
})

/*Update Category Data*/
router.put('/categories', (req, res) => {
    var body = req.body;
    model.user_category.update({
        user_category_name: body.category_name,
        module_ids: body.module_ids
    }, {
        where: {
            id: body.user_category_id
        }
    }).then(data => {
        if (data > 0) {
            res.json({
                stat: 'success',
                code: 200,
                id: data.id
            });
        } else {
            res.json({
                stat: 'nodata',
                code: 302
            });
        }
    }, error => {
        res.json({
            stat: 'failure',
            code: 300,
            message: error
        })
    })
})

/*Create a new generic user*/
/*Modified profile_pic_path*/
router.post('/generic', upload.single('profile_pic'), (req, res) => {
    var body = req.body;
    model.user.create({
        name: body.name,
        email_id: body.email_id,
        password: passwordHash.generate(body.password),
        mobile_number: body.mobile_no,
        phone_no: body.phone_no,
        profile_pic_path: req.file.filename,
        active: body.is_active,
        userCategoryId: body.user_categoryid
    }).then(data => {
        res.json({
            id: data.id,
            stat: 'success',
            code: 200
        })
    }, error => {
        res.json({
            stat: 'failure',
            code: 301,
            message: error
        })
    })
})

/*Create individual user*/
/*Modified Image Upload*/
/* Transaction added */
router.post('/individual', upload.single('profile_pic'), (req, res) => {
    var body = req.body;
    var userId=-1;
    var auxId=-1;
    model.sequelize.transaction(function(t) {
        return model.user.create({
            name: body.name,
            email_id: body.email_id,
            password: passwordHash.generate(body.password),
            mobile_number: body.mobile_no,
            phone_no: body.phone_no,
            profile_pic_path: req.file.filename,
            active: body.is_active,
            userCategoryId: body.user_categoryid
        },{
            transaction:t
        }).then(data => {
            userId=data.id;
            return model.individual_user_auxilliary.create({
                pan: body.pan,
                pan_verified: body.pan_verified,
                email_hash: body.email_hash,
                email_verified: body.email_verified,
                userId: data.id
            },
            {
                transaction:t
            }).then(data2 => {
                auxId=data2.id;
            })
        })
    }).then(result=>{
        res.json({
            stat:'success',
            code:200,
            user_id:userId,
            aux_data_id:auxId
        })
    }).catch(error=>{
        res.json({
            stat:'failure',
            code:300,
            error:error
        })
    })
})

/*Get a specific individual user*/
router.get('/individual/:id', (req, res) => {
    model.user.findOne({
        where: {
            id: req.params.id
        },
        attributes: {
            exclude: ['password']
        },
        include: [{
            model: model.user_category,
            as: 'user_category',
            attributes: ['user_category_name']
        }, model.individual_user_auxilliary]
    }).then(data => {
        res.json({
            user: data
        })
    }, error => {
        console.log(error);
        res.json({
            stat: 'failure',
            code: 301
        })
    })
})

/*Corporate User*/
/*Modified image upload*/
/* Transaction added */
router.post('/corporate', upload.single('profile_pic'), (req, res) => {
    var body = req.body;
    var userId=-1;
    var auxId=-1;
    model.sequelize.transaction(function(t) {
        return model.user.create({
            name: body.name,
            email_id: body.email_id,
            password: passwordHash.generate(body.password),
            mobile_number: body.mobile_no,
            phone_no: body.phone_no,
            profile_pic_path: req.file.filename,
            active: body.is_active,
            userCategoryId: body.user_categoryid
        },{
            transaction: t
        }).then(data => {
            userId=data.id;
            return model.retail_corp_user_auxilliary.create({
                pan: body.pan,
                pan_verified: body.pan_verified,
                vat_num: body.vat_num,
                st_num: body.st_num,
                tan_num: body.tan_num,
                payment_cycle: body.payment_cycle,
                userId: data.id
            },{
                transaction: t
            }).then(data2 => {
                auxId=data2.id
            })
        })
    }).then(result=>{
        /*res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");*/
        res.json({
            stat:'success',
            code:200,
            user_id:userId,
            aux_data_id: auxId
        })
    }).catch(error=>{
        res.json({
            stat: 'failure',
            code: 301,
            error: error
        })
    });
})

/*Get specific corporate user*/
router.get('/corporate/:id', (req, res) => {
    model.user.findOne({
        where: {
            id: req.params.id
        },
        attributes: {
            exclude: ['password']
        },
        include: [{
            model: model.user_category,
            as: 'user_category',
            attributes: ['user_category_name']
        }, model.retail_corp_user_auxilliary]
    }).then(data => {
        res.json({
            user: data
        })
    }, error => {
        console.log(error);
        res.json({
            stat: 'failure',
            code: 301
        })
    })
})

/*Create a fleet user*/
/*Modified image upload*/
/* Transaction added */
router.post('/fleetowner', upload.single('profile_pic'), (req, res) => {
    var body = req.body;
    var userId=-1;
    var auxId=-1;
    model.sequelize.transaction(function(t) {
      return model.user.create({
          name: body.name,
          email_id: body.email_id,
          password: passwordHash.generate(body.password),
          mobile_number: body.mobile_no,
          phone_no: body.phone_no,
          profile_pic_path: req.file.filename,
          active: body.is_active,
          userCategoryId: body.user_categoryid
      }, {
          transaction: t
      }).then(data => {
          userId=data.id;
          return model.fleet_owner_auxilliary.create({
              bank_name: body.bank_name,
              bank_branch: body.bank_branch,
              bank_accntNo: body.bank_accntNo,
              bank_ifsc: body.bank_ifsc,
              tds_status: body.tds_status,
              payment_period: body.payment_period,
              userId: data.id
          }, {
              transaction:t
          }).then(data2=>{
              auxId=data2.id;
          })
      })
    }).then(result => {
      /*res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");*/
      res.json({
          stat:'success',
          code: 200,
          user_id:userId,
          aux_data_id: auxId
      })
    }).catch(error => {
        console.log(error);
      res.json({
          stat:'failure',
          code: 300,
          error: error
      });
    });
});
/*Get all fleetowner*/
/*router.get('/fleetowner', (req, res) => {

})*/

/* Update active status */
router.put('/setActive/:id',(req,res)=>{
    model.user.update({
        active:req.body.active,
        mobile_number: req.body.mobile_no
    },{
        where:{
            id:req.params.id
        }
    }).then(data=>{
        if(data==0){
            res.json({
                stat:'nodata',
                code:302
            })
        }else{
            res.json({
                stat:'success',
                code:300
            })
        }
    }, error=>{
        res.json({
            stat:'failure',
            code:300,
            error:error
        })
    })
})


/*Update Rating*/
router.post('/rating', (req, res) => {
    var body = req.body;
    model.user.findOne({
        where: {
            id: body.user_id
        },
        attributes: ['rating', 'rating_count']
    }).then(data => {
        if (data == null) {
            res.json({
                stat: 'nodata',
                code: 302
            });
        } else {
            var rating = data.rating;
            var rating_count = data.rating_count;
            var cur_tot = (rating * rating_count) + body.rating;
            var avg = cur_tot / (rating_count + 1);
            model.user.update({
                rating: avg,
                rating_count: rating_count + 1
            }, {
                where: {
                    id: body.user_id
                }
            }).then(data1 => {
                res.json({
                    stat: 'success',
                    code: 200,
                    updated_rating: data1.rating,
                    id: data1.id
                })
            }, error1 => {
                res.json({
                    stat: 'failure',
                    code: 300,
                    error: error1
                })
            })
        }
    }, error => {
        res.json({
            stat: 'failure',
            code: 300,
            error: error1
        })
    })
})

/*Get a specific fleet owner*/
router.get('/fleetowner/:id', (req, res) => {
    model.user.findOne({
        where: {
            id: req.params.id
        },
        attributes: {
            exclude: ['password']
        },
        include: [{
            model: model.user_category,
            as: 'user_category',
            attributes: ['user_category_name']
        }, {
            model: model.fleet_owner_auxilliary
        }, model.driver, model.vehicle]
    }).then(data => {
        res.json({
            user: data
        })
    }, error => {
        console.log(error);
        res.json({
            stat: 'failure',
            code: 301
        })
    })
})

/*Login */
router.post('/login', (req, res) => {
    var mobile_no = req.body.mobile_no;
    var password = req.body.password;
    model.user.findOne({
        attributes: ['name', 'id', 'password', 'userCategoryId','active'],
        where: {
            mobile_number: mobile_no
        }
    }).then(data => {
        if (data != null) {
            if (passwordHash.verify(password, data.password)) {
                res.json({
                    stat: 'success',
                    id: data.id,
                    category_id: data.userCategoryId,
                    mobile_no: mobile_no,
                    active:data.active,
                    code: 100
                });
            } else {
                res.json({
                    stat: 'failure',
                    id: data.id,
                    message: 'wrong password',
                    code: 203
                });
            }
        } else {
            res.json({
                stat: 'nodata',
                code: 400
            });
        }
    }, err => {
        res.json({
            stat: 'error',
            code: 500
        });
        console.log(err.errMsg);
    })
})

/* Get users by category */
router.get('/bycategory/:cat_id', (req, res) => {
    model.user_category.findOne({
        where: {
            id: req.params.cat_id
        }
    }).then(xdata => {
        if (xdata != null) {
            model.user.findAll({
                where: {
                    userCategoryId: req.params.cat_id
                },
                attributes: {
                    exclude: ['password']
                },
                include: [{
                    model: model.user_category,
                    as: 'user_category',
                    attributes: ['user_category_name']
                } /*, model.fleet_owner_auxilliary*/ ]
            }).then(data => {
                res.json({
                    user_count: data.length,
                    users: data
                })
            }, error => {
                console.log(error);
                res.json({
                    stat: 'failure',
                    code: 301
                })
            })
        } else {
            res.json({
                stat: 'nodata',
                code: 303
            })
        }
    }, error => {
        res.json({
            stat: 'failure',
            code: 400
        })
    })
});
/* Get all vehicles */
router.get('/vehicles/:id', (req,res)=>{
	var user_id=req.params.id;
	model.user.findOne({
		where:{
			id: req.params.id
		},
		attributes:['id','userCategoryId'],
		include:[model.driver,model.vehicle]
	}).then(data=>{
		if(data==null){
			res.json({
				stat:'nodata',
				code:302
			});
		}else{
			res.json({
				stat:'success',
				code:200,
				data:data
			});
		}
	}, error=>{
		res.json({
			stat:'failure',
			code: 300,
			error:error
		});
	});
});

/* Update User Password */
router.put('/password/:id', (req, res) => {
    var body = req.body;
    var password = body.current_password;
    var newPass = body.new_password;
    model.user.findOne({
        where: {
            id: req.params.id
        },
        attributes: ['name', 'id', 'password']
    }).then(data => {
        if (data != null) {
            //check for validation
            if (passwordHash.verify(password, data.password)) {
                model.user.update({
                    password: passwordHash.generate(newPass)
                }, {
                    where: {
                        id: req.params.id
                    }
                }).then(data2 => {
                    if (data2 != null) {
                        res.json({
                            stat: 'success',
                            code: 200
                        })
                    } else {
                        res.json({
                            stat: 'nodata',
                            code: 302
                        })
                    }
                }, error2 => {

                });
            }else{
              res.json({
                  stat: 'error',
                  message:'wrong password',
                  code: 303
              })
            }
        } else {
            res.json({
                stat: 'nodata',
                code: 302
            })
        }
    }, error => {
        res.json({
            stat: 'failure',
            code: 300,
            error: error
        })
    });
});

/* Update User Profile_pic */
/*Modified Image Upload*/
router.put('/profile_pic/:id', (req, res) => {
    var body = req.body;
    model.user.update({
        profile_pic_path: body.profile_pic_path
    }, {
        where: {
            id: req.params.id
        }
    }).then(data => {
        if (data <= 0) {
            res.json({
                stat: 'nodata',
                code: 302
            })
        } else {
            res.json({
                stat: 'success',
                code: 200
            })
        }
    }, error => {
        res.json({
            stat: 'error',
            code: 300,
            error: error
        })
    });
});
/*Update Phone No*/
router.put('/phone/:id', (req, res) => {
    var body = req.body;
    model.user.update({
        phone_no: body.phone_no
    }, {
        where: {
            id: req.params.id
        }
    }).then(data => {
        if (data <= 0) {
            res.json({
                stat: 'nodata',
                code: 302
            })
        } else {
            res.json({
                stat: 'success',
                code: 200
            })
        }
    }, error => {
        res.json({
            stat: 'error',
            code: 300,
            error: error
        })
    });
});
/*Update Mobile No*/
router.put('/mobile/:id', (req, res) => {
    var body = req.body;
    model.user.update({
        mobile_number: body.mobile_number
    }, {
        where: {
            id: req.params.id
        }
    }).then(data => {
        if (data <= 0) {
            res.json({
                stat: 'nodata',
                code: 302
            })
        } else {
            res.json({
                stat: 'success',
                code: 200
            })
        }
    }, error => {
        res.json({
            stat: 'error',
            code: 300,
            error: error
        })
    });
});
/*Update Name*/
router.put('/name/:id', (req, res) => {
    var body = req.body;
    model.user.update({
        name: body.name
    }, {
        where: {
            id: req.params.id
        }
    }).then(data => {
        if (data <= 0) {
            res.json({
                stat: 'nodata',
                code: 302
            })
        } else {
            res.json({
                stat: 'success',
                code: 200
            })
        }
    }, error => {
        res.json({
            stat: 'error',
            code: 300,
            error: error
        })
    });
});
/*Update Email Id*/
router.put('/email/:id', (req, res) => {
    var body = req.body;
    model.user.update({
        email_id: body.email_id
    }, {
        where: {
            id: req.params.id
        }
    }).then(data => {
        if (data <= 0) {
            res.json({
                stat: 'nodata',
                code: 302
            })
        } else {
            res.json({
                stat: 'success',
                code: 200
            })
        }
    }, error => {
        res.json({
            stat: 'error',
            code: 300,
            error: error
        })
    });
});

/* Aux table update apis*/
/*FleetOwner*/
router.put('/fleetowner/aux/:id', (req,res)=>{
    var aux=req.body;
    delete aux.id;
    delete aux.createdAt;
    delete aux.updatedAt;
    delete aux.userId;
    var userid=req.params.id;
    model.fleet_owner_auxilliary.update(aux,{
        where:{
            userId:userid
        }
    }).then(data => {
        if (data <= 0) {
            res.json({
                stat: 'nodata',
                code: 302
            })
        } else {
            res.json({
                stat: 'success',
                code: 200,
                updated:Object.keys(aux)
            })
        }
    }, error => {
        res.json({
            stat: 'error',
            code: 300,
            error: error
        })
    });
});
/*Individual*/
router.put('/individual/aux/:id', (req,res)=>{
    var aux=req.body;
    delete aux.id;
    delete aux.createdAt;
    delete aux.updatedAt;
    delete aux.userId;
    var userid=req.params.id;
    model.individual_user_auxilliary.update(aux,{
        where:{
            userId:userid
        }
    }).then(data => {
        if (data <= 0) {
            res.json({
                stat: 'nodata',
                code: 302
            })
        } else {
            res.json({
                stat: 'success',
                code: 200,
                updated:Object.keys(aux)
            })
        }
    }, error => {
        res.json({
            stat: 'error',
            code: 300,
            error: error
        })
    });
});
/*Corporate*/
router.put('/corporate/aux/:id', (req,res)=>{
    var aux=req.body;
    delete aux.id;
    delete aux.createdAt;
    delete aux.updatedAt;
    delete aux.userId;
    var userid=req.params.id;
    model.retail_corp_user_auxilliary.update(aux,{
        where:{
            userId:userid
        }
    }).then(data => {
        if (data <= 0) {
            res.json({
                stat: 'nodata',
                code: 302
            })
        } else {
            res.json({
                stat: 'success',
                code: 200,
                updated:Object.keys(aux)
            })
        }
    }, error => {
        res.json({
            stat: 'error',
            code: 300,
            error: error
        })
    });
});
/*Update user data*/
router.put('/update/:id', (req,res)=>{
    var body=req.body;
    delete body.id;
    delete body.profile_pic_path;
    delete body.userCategoryId;
    delete body.rating;
    delete body.rating_count;
    model.user.update(body,{
        where:{
            id:req.params.id
        }
    }).then(data=>{
        if (data <= 0) {
            res.json({
                stat: 'nodata',
                code: 302
            })
        } else {
            res.json({
                stat: 'success',
                code: 200,
                updated:Object.keys(body)
            })
        }
    }, error => {
        res.json({
            stat: 'error',
            code: 300,
            error: error
        })
    });
});
/*Phone exists*/
router.get('/exists/phone/:no', (req,res)=>{
    model.user.count({
        where:{
            mobile_number:req.params.no
        }
    }).then(c=>{
        res.json({
            stat:'success',
            code:200,
            data:(c>0)?true:false
        })
    }, error=>{
        res.json({
            stat:'failure',
            code:300,
            error:error
        })
    })
});
module.exports = router;
