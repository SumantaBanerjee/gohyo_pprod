module.exports=(sequelize, Sequelize) =>{
  var Rejection=sequelize.define("rejection",{
    rejection_remark:{
      type: Sequelize.STRING,
      allowNull: false
    }
  });
  return Rejection;
}
