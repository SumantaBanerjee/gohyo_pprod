module.exports = (sequelize, Sequelize) => {
    var Order = sequelize.define("order", {
        mode_of_payment: {
            type: Sequelize.STRING,
            defaultValue: 'Cash'
        },
        delivery_location: {
            type: 'point',
            defaultValue: sequelize.literal('point(0,0)')
        },
        pickup_location: {
            type: 'point',
            defaultValue: sequelize.literal('point(0,0)')
        },
        billing_address: {
            type: Sequelize.TEXT
        },
        consignment_value: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        weight: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        no_of_units: {
          type: Sequelize.INTEGER,
          allowNull: false,
          defaultValue: 0
        },
        trial_count: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 0
        },
        distance_travelled: {
            type: Sequelize.FLOAT,
            allowNull: false,
            defaultValue: 0
        },
        timetaken: {
            type: Sequelize.BIGINT,
            allowNull: false,
            defaultValue: 0
        },
        insuranceRequired: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },
        labourRequired: {
            type: Sequelize.BOOLEAN,
            defaultValue: false
        },
        shipment_health:{
          type: Sequelize.STRING
        },
        feedback:{
          type: Sequelize.TEXT,
          defaultValue: ''
        },
        shipment_photo_path:{
          type: Sequelize.STRING(255),
          default: ''
        },
        digital_signature_path:{
          type: Sequelize.STRING(255),
          default:''
        },
        order_number:{
            type: Sequelize.STRING(20)
        },
        vehicleCategoryId:{
            type: Sequelize.INTEGER
        },
        totalbill:{
            type: Sequelize.INTEGER,
            defaultValue:0
        },
        estimatedbill:{
            type: Sequelize.INTEGER,
            defaultValue:0
        },
        cn_name:{
            type: Sequelize.STRING(255)
        },
        cn_mobile_no:{
            type:Sequelize.STRING(255)
        },
        orderStatusId:{
            type:Sequelize.INTEGER
        },
        customer_invoice_no:{
            type: Sequelize.STRING(100)
        },
        remarks:{
            type: Sequelize.TEXT
        }
    });
    return Order;
}
