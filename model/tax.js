module.exports=(sequelize, Sequelize) =>{
  var Tax=sequelize.define("tax",{
    tax_amt:{
      type:Sequelize.DECIMAL(5,2)
    },
    valid_from:{
      type:Sequelize.DATEONLY,
      defaultValue: Sequelize.NOW
    }
  });
  return Tax;
}
