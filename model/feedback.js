module.exports=(sequelize, Sequelize) =>{
  var Feedback=sequelize.define("feedback_master",{
    user_feedback:{
      type:Sequelize.STRING(255),
      allowNull: true
    },
    user_rating:{
      type: Sequelize.DECIMAL(2,1),
      allowNull: false
    },
    driver_feedback:{
      type:Sequelize.STRING(255),
      allowNull: true
    },
    driver_rating:{
      type: Sequelize.DECIMAL(2,1),
      allowNull: false
    }
  },{
    freezeTableName: true
  });
  return Feedback;
}
