module.exports=(sequelize, Sequelize) =>{
  var CRateChart=sequelize.define("corp_rate_chart",{
    source:{
      type: Sequelize.STRING,
      allowNull: false
    },
    destination:{
      type: Sequelize.STRING,
      allowNull: false
    },
    vehicle_type:{
      type: Sequelize.STRING,
      allowNull: false,
      defaultValue: ''
    },
    trip_distance:{
      type: Sequelize.DECIMAL(10,2),
      defaultValue: 0.0
    },
    rate_per_km:{
      type: Sequelize.DECIMAL(5,2),
      defaultValue: 0.0
    },
    total_rate:{
      type: Sequelize.DECIMAL(15,2),
      defaultValue: 0.0
    }
  });
  return CRateChart;
}
