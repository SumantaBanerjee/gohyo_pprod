module.exports=(sequelize, Sequelize) =>{
  var Item=sequelize.define("item",{
    invoice_number: {
      type: Sequelize.STRING,
      allowNull: false
    },
    stn_no:{
      type: Sequelize.STRING,
      allowNull: false
    },
    no_of_units: {
      type: Sequelize.INTEGER,
      allowNull: false
    }
  });
  return Item;
}
