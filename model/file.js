module.exports=(sequelize, Sequelize) =>{
  var File=sequelize.define("file",{
    file_name:{
      type: Sequelize.STRING,
      allowNull: false
    },
    file_path:{
      type: Sequelize.STRING(255),
      allowNull: false
    }
  });
  return File;
}
