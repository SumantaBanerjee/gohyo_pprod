module.exports=(sequelize, Sequelize) =>{
  var Timestamp=sequelize.define("timestamp",{
    booking_timestamp:{ //1
      type: Sequelize.DATE
    },
    trip_active:{
      type: Sequelize.DATE
    },
    loading_destinationReached_ts:{ //2
      type: Sequelize.DATE
    },
    loading_complete_ts:{ //3
      type:Sequelize.DATE
    },
    unloading_destnReached_ts:{ //4
      type: Sequelize.DATE
    },
    unloading_complete_ts:{ //5
      type:Sequelize.DATE
    },
    shipment_complete_ts:{ //6
      type:Sequelize.DATE
    },
    currentStatus:{
      type:Sequelize.INTEGER,
      defaultValue:0
    },
    tripComplete:{
      type:Sequelize.BOOLEAN,
      defaultValue: false
    },
    payment_loading_timestamp:{
      type:Sequelize.DATE
    },
    payment_unloading_timestamp:{
      type:Sequelize.DATE
    }
  });
  return Timestamp;
}
