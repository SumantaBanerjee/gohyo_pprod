module.exports=(sequelize, Sequelize) =>{
  var literal = "ST_GeographyFromText('point(0 0)')"
  var point = {
      type: 'Point',
      coordinates: [0,0],
      crs: {
          type: 'name',
          properties: {
              name: 'EPSG:4326'
          }
      }
  };
  var Zone=sequelize.define("zone",{
    zone_name:{
      type: Sequelize.STRING,
      allowNull:'unique_zone',
    },
    zone_location:{
      type: 'GEOGRAPHY',
      defaultValue: sequelize.literal(literal),
      allowNull: false
    },
    adjacent_zones:{
      type: Sequelize.TEXT,
      allowNull: true
    },
    loading_time:{
      type: Sequelize.INTEGER,
      defaultValue: 0
    },
    unloading_time:{
      type: Sequelize.INTEGER,
      defaultValue: 0
    },
    rate_per_minute:{
      type:Sequelize.INTEGER,
      defaultValue:1
    },
    pincode:{
        type:Sequelize.STRING(7)
    },
    district:{
        type:Sequelize.STRING(255)
    },
    city:{
        type:Sequelize.STRING(255)
    }
  });
  return Zone;
}
