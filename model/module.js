module.exports=(sequelize, Sequelize) =>{
  var Module=sequelize.define("module",{
    module_name:{
      type: Sequelize.STRING,
      allowNull: false
    },
    controller_name:{
        type: Sequelize.STRING(255)
    }
  });
  return Module;
}
