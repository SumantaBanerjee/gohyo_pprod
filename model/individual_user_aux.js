module.exports = (sequelize, Sequelize) => {
    var UserAux = sequelize.define("individual_user_auxilliary", {
        pan: {
            type: Sequelize.STRING,
            allowNull: false
        },
        pan_verified:{
          type: Sequelize.BOOLEAN,
          defaultValue: false
        },
        email_hash:{
          type: Sequelize.STRING,
          defaultValue: ''
        },
        email_verified:{
          type: Sequelize.BOOLEAN,
          defaultValue: false
        }
    }, {
        freezeTableName: true
    });
    return UserAux;
}
