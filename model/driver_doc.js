module.exports=(sequelize, Sequelize) =>{
  var DriverDoc=sequelize.define("driver_doc",{
    doc_name:{
      type: Sequelize.STRING(255),
      allowNull: false
    },
    doc_path:{
      type: Sequelize.STRING,
      allowNull: false
    }
  });
  return DriverDoc;
}
