module.exports=(sequelize, Sequelize) =>{
  var Role=sequelize.define("role",{
    role_name:{
      type: Sequelize.STRING,
      allowNull: false
    },
    module_ids:{
      type: Sequelize.STRING,
      allowNull: false
    },
    controller_name:{
        type: Sequelize.STRING(255)
    }
  });
  return Role;
}
