module.exports=(sequelize, Sequelize) =>{
  var UserCategory=sequelize.define("user_category",{
    user_category_name:{
      type: Sequelize.STRING,
      unique: 'composite_category_modules',
      allowNull: false
    },
    module_ids:{
      type: Sequelize.STRING,
      unique: 'composite_category_modules',
      allowNull: false
    }
  },{
    freezeTableName: true
  });
  return UserCategory;
}
