module.exports=(sequelize, Sequelize) =>{
  var Driver=sequelize.define("driver",{
    driver_name:{
      type: Sequelize.STRING(200),
      allowNull: false
    },
    driver_address:{
      type: Sequelize.STRING(255),
      allowNull: true
    },
    driver_mobile:{
      type: Sequelize.STRING(12),
      allowNull:false
    },
    rating:{
      type: Sequelize.DECIMAL(2,1),
      allowNull: false,
      defaultValue: 0.0
    },
    rating_count:{
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue:0
    },
    driver_photo_path:{
      type: Sequelize.STRING(255),
      defaultValue: 'http://s3.amazonaws.com/37assets/svn/765-default-avatar.png'
    },
    rejections:{
        type: Sequelize.INTEGER
    }
  });
  return Driver;
}
