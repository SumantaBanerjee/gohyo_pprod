module.exports=(sequelize, Sequelize) =>{
  var Document=sequelize.define("document",{
    file_name:{
      type: Sequelize.STRING,
      allowNull: false
    },
    file_path:{
      type: Sequelize.STRING(255),
      allowNull: false
    },
    expiry_date: {
        type: Sequelize.DATEONLY
    }
  });
  return Document;
}
