module.exports=(sequelize, Sequelize) =>{
  var Slab=sequelize.define("slab",{
    slab_lower_limit:{
      type: Sequelize.INTEGER,
      allowNull: false
    },
    slab_upper_limit:{
      type: Sequelize.INTEGER,
      allowNull: false
    },
    rate: {
      type: Sequelize.INTEGER,
      allowNull: false
    }
  });
  return Slab;
}
