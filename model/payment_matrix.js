module.exports=(sequelize, Sequelize) =>{
  var Payment_Matrix=sequelize.define("payment_matrix",{
    payment_type:{
      type: Sequelize.STRING
    },
    payment_loading:{
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    payment_unloading:{
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    payment_loading_mode:{
      type: Sequelize.STRING,
      allowNull: true
    },
    payment_unloading_mode:{
      type: Sequelize.STRING,
      allowNull: true
    },
    payment_loading_status:{
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    payment_unloading_status:{
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
  });
  return Payment_Matrix;
}
