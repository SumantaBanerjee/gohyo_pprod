module.exports = (sequelize, Sequelize) => {
    var UserAux = sequelize.define("retail_corp_user_auxilliary", {
        pan: {
            type: Sequelize.STRING,
            allowNull: false
        },
        pan_verified:{
          type: Sequelize.BOOLEAN,
          defaultValue: false
        },
        vat_num:{
          type: Sequelize.STRING,
          allowNull: false
        },
        st_num:{
          type: Sequelize.STRING,
          allowNull: false
        },
        tan_num:{
          type: Sequelize.STRING,
          allowNull: false
        },
        payment_cycle:{
          type:Sequelize.INTEGER,
          allowNull: false,
          defaultValue: 30
        }
    }, {
        freezeTableName: true
    });
    return UserAux;
}
