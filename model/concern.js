module.exports=(sequelize, Sequelize) =>{
  var Concern=sequelize.define("concern",{
      concern:{
          type: Sequelize.TEXT
      },
      concern_type:{
          type:Sequelize.STRING(20)
      }
  });
  return Concern;
}
