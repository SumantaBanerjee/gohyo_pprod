module.exports=(sequelize, Sequelize) =>{
  var Address=sequelize.define("address",{
    pickup_address:{
      type: Sequelize.TEXT
    },
    pickup_address_tag:{
      type: Sequelize.STRING
    },
    drop_address:{
      type: Sequelize.TEXT
    },
    drop_address_tag:{
      type: Sequelize.STRING
    },
    corporate_rate:{
        type: Sequelize.INTEGER,
        defaultValue:0
    }
  });
  return Address;
}
