module.exports=(sequelize, Sequelize) =>{
  var Scheme=sequelize.define("scheme",{
    scheme_name:{
      type: Sequelize.STRING,
      allowNull: false
    },
    is_active:{
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: true
    },
    scheme_creator: {
      type: Sequelize.STRING,
      allowNull: false
    }
  });
  return Scheme;
}
