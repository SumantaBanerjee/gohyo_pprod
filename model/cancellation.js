module.exports=(sequelize, Sequelize) =>{
  var Cancellation=sequelize.define("cancellation",{
      reason:{
          type: Sequelize.TEXT
      },
      remarks:{
          type:Sequelize.TEXT
      }
  });
  return Cancellation;
}
