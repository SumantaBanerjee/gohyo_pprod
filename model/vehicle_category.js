module.exports=(sequelize, Sequelize) =>{
  var VehicleCategory=sequelize.define("vehicle_category",{
    category_name:{
      type: Sequelize.STRING,
      unique: 'vehicle_category_modules',
      allowNull: false
    },
    category_type:{
      type: Sequelize.STRING
    },
    category_rate:{
      type: Sequelize.INTEGER,
      defaultValue: 0
    },
    category_booking_radius:{
      type: Sequelize.INTEGER,
      defaultValue:0,
      comment: "Distance in meters"
    }
  },{
    freezeTableName: true
  });
  return VehicleCategory;
}
