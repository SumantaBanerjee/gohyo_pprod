var Sequelize=require('sequelize');
var config=require('config');
var conString=config.get('conString');
var fs        = require("fs");
var path      = require("path");
var sequelize = new Sequelize(conString);
var db        = {};

fs
  .readdirSync(__dirname)
  .filter(function(file) {
    return (file.indexOf(".") !== 0) && (file !== "index.js");
  })
  .forEach(function(file) {
    var model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(function(modelName) {
  if ("associate" in db[modelName]) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

/* Define Relations */
db.order.belongsTo(db.vehicle);
db.order.belongsTo(db.driver);
db.driver.hasMany(db.order);
db.vehicle.hasMany(db.order);
db.driver_doc.belongsTo(db.driver);
db.driver.hasMany(db.driver_doc);
db.timestamp.belongsTo(db.order);
db.order.hasOne(db.timestamp);
db.invoice_header.belongsTo(db.order);
db.invoice_header.belongsTo(db.invoice);
db.rejection.belongsTo(db.order);
db.order.hasOne(db.rejection);
db.order.hasMany(db.item);
db.item.belongsTo(db.order);
db.order.hasOne(db.payment_matrix);
db.payment_matrix.belongsTo(db.order);

db.driver.hasOne(db.vehicle, {foreignKey:'driverId'});
db.vehicle.belongsTo(db.driver, {foreignKey:'driverId'});

db.vehicle.belongsTo(db.user, {foreignKey:'fleetOwnerId'});

db.user.hasMany(db.order);
db.order.belongsTo(db.user);

db.user.hasMany(db.address);
db.address.belongsTo(db.user);

db.concern.belongsTo(db.order);
db.order.hasOne(db.concern);
db.concern.belongsTo(db.user);
db.concern.belongsTo(db.driver);

db.zone.hasOne(db.vehicle);
db.vehicle.belongsTo(db.zone);
db.vehicle.belongsTo(db.vehicle_category,{foreignKey:'vehicleCategoryId'});
db.vehicle_category.hasMany(db.vehicle,{foreignKey:'vehicleCategoryId'});

db.driver.belongsTo(db.user,{foreignKey:'fleetOwnerId'});
db.user.hasMany(db.driver,{foreignKey:'fleetOwnerId'});

db.trip_daily.belongsTo(db.order);
db.order.hasOne(db.trip_daily);

db.cancellation.belongsTo(db.order);
db.order.hasOne(db.cancellation, {foreignKey:'orderId'});

db.document.belongsTo(db.user);
db.user.hasMany(db.document, {foreignKey:'userId'});
db.document.belongsTo(db.driver);
db.driver.hasMany(db.document, {foreignKey:'driverId'});
db.document.belongsTo(db.vehicle);
db.vehicle.hasMany(db.document, {foreignKey:'vehicleId'});

db.user.hasMany(db.address);
db.address.belongsTo(db.user);
db.user.belongsTo(db.user_category);
db.individual_user_auxilliary.belongsTo(db.user);
db.user.hasOne(db.individual_user_auxilliary);
db.retail_corp_user_auxilliary.belongsTo(db.user);
db.user.hasOne(db.retail_corp_user_auxilliary);
db.scheme.hasMany(db.slab);
db.retail_corp_user_auxilliary.belongsTo(db.user);
db.corp_rate_chart.belongsTo(db.user);

db.user.hasOne(db.fleet_owner_auxilliary);
db.fleet_owner_auxilliary.belongsTo(db.user);

db.user.hasMany(db.feedback_master);
db.feedback_master.belongsTo(db.user);
db.driver.hasMany(db.feedback_master);
db.feedback_master.belongsTo(db.driver);
db.feedback_master.belongsTo(db.order);
db.order.hasOne(db.feedback_master);

db.vehicle.belongsTo(db.scheme);
db.vehicle.hasOne(db.driver_vehicle_pair);
db.driver.hasOne(db.driver_vehicle_pair);
db.user.hasOne(db.driver_vehicle_pair,{foreignKey:'fleetOwnerId'});
db.driver_vehicle_pair.belongsTo(db.driver);
db.driver_vehicle_pair.belongsTo(db.vehicle);
db.driver_vehicle_pair.belongsTo(db.user,{foreignKey:'fleetOwnerId'});
db.user.hasMany(db.vehicle, {foreignKey: 'fleetOwnerId'});
db.trip_daily.belongsTo(db.vehicle);
module.exports = db;
