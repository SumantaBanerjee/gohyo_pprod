module.exports=(sequelize, Sequelize) =>{
  var TripDaily=sequelize.define("trip_daily",{
    trip_distance:{
      type: Sequelize.DECIMAL(10,2),
      allowNull: false,
      defaultValue: 0.0
    },
    empty_distance:{
      type: Sequelize.DECIMAL(10,2),
      allowNull: false,
      defaultValue: 0.0
    }
  });
  return TripDaily;
}
