var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var model = require('./model');
var modules = require('./controller/modules');
var users = require('./controller/users');
var schemes = require('./controller/schemes');
var files = require('./controller/files');
var vehicles = require('./controller/vehicles');
var drivers = require('./controller/drivers');
var orders = require('./controller/orders');
var taxes = require('./controller/taxes');
var zones = require('./controller/zones');
var roles = require('./controller/roles');
var addresses = require('./controller/addresses');
var documents = require('./controller/documents');
var concern = require('./controller/concerns');
var app = express();
var port=3000;
var authenticate = (req, res, next) => {
    /*
    //var appId=
    */
    //res.send('http://google.co.in');
    next();
    return;
}
app.use(authenticate);
//app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use('/file', express.static('uploads'));
app.use('/docs', express.static('documents'));
app.use('/modules', modules);
app.use('/users', users);
app.use('/schemes', schemes);
app.use('/files', files);
app.use('/vehicles', vehicles);
app.use('/drivers', drivers);
app.use('/orders', orders);
app.use('/taxes', taxes);
app.use('/zones', zones);
app.use('/roles', roles);
app.use('/addresses', addresses);
app.use('/documents', documents);
app.use('/concerns', concern);
/*Get Weight List*/
app.get('/weight_list',(req,res)=>{
  res.json({weight_list:['1-100','101-1000','1001-1500','1501-2000']})
})
/* Goods categories*/
app.get('/goods_categories',(req,res)=>{
  res.json({category_list:['Industrial Goods','Electricals','Perishables']})
})
console.log("Syncing database...")
model.sequelize.sync({
    //logging: console.log//,
    //force: true
}).then(function() {
    model.user_category.bulkCreate([{
        user_category_name: 'Individual',
        module_ids: ""
    }, {
        user_category_name: 'Corporate',
        module_ids: ""
    }, {
        user_category_name: 'Fleetowner',
        module_ids: ""
    }, {
        user_category_name: 'Retail',
        module_ids: ""
    }]).then(data => {
        console.log("Starting up server");
        app.listen(port, function(d) {
            console.log("Server started");
        });
    },error=>{
      if(error instanceof model.Sequelize.ValidationError){
        console.log("Not inserting duplicate category values");
        console.log("Starting up server");
        app.listen(port, function(d) {
            console.log("Server started at port "+port);
        });
      }else{
        console.log(error);
      }
    })

});